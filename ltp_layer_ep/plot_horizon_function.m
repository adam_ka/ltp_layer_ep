%% fixed horizon
x1(1) = 0;
x = (1:14:500)';

horizon_days = 90;
horizon_slope = 60;
horizon_max_drop = 0.2; %allowed maximum drop in MM contribution within horizon
horizon_correction_for_slope = -(-horizon_slope*log(1/horizon_max_drop-1));
horizon_factor = 1-1./(1+exp(-(x-x1(end)-horizon_days-horizon_correction_for_slope)/horizon_slope));

% Equivalent annonymous function for use with LTPModel class
h = @(x,x_end) 1-1./(1+exp(-(x(:)-x_end-90-(-(-60*log(1/0.2-1))))/60));

figure
plot(x,horizon_factor,'DisplayName','Weight function')
hold on
plot(x,h(x,x1(end)),'o','DisplayName','Weight function annonymous')
plot([horizon_days horizon_days],[0 1],'r--',...
    'DisplayName',['MM Prediction Horizon, until ' num2str(horizon_max_drop*100) '% drop'])

legend('show')
xlabel('Days from AgeToday')
ylabel('Weight on random response')



%% variable horizon

x1_AGETODAY = [0 200 400];
x = (1:700)';

figure
for x1 = x1_AGETODAY
    horizon_days = 90+x1/500*90;
    horizon_slope = 60/90*horizon_days;
    horizon_max_drop = 0.2; %allowed maximum drop in MM contribution within horizon   
    horizon_correction_for_slope = -(-horizon_slope*log(1/horizon_max_drop-1));
    horizon_factor = 1-1./(1+exp(-(x-x1-horizon_days-horizon_correction_for_slope)/horizon_slope));
    plot(x,horizon_factor,'DisplayName',['AgeToday=' num2str(x1)])
    hold on
    %plot([x1+horizon_days x1+horizon_days],[0 1],'k--','DisplayName',['Horizon for AgeToday=' num2str(x1)])    
end
plot(x,x*0+1-horizon_max_drop,'DisplayName','Horizon threshold')
legend('show')
xlabel('Age (EP aligned) (Days)')
ylabel('Weight on random response')

grid on
