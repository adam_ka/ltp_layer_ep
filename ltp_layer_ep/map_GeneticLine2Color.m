function data = map_GeneticLine2Color(data,filename,stopIfUnknownLine)
% map_GeneticLine2Color - assign egg color based on genetic line
% 
% INPUT
%   data - a table with required column 'line'
%   filename - xls file with columns containing short line names (col A), long line names (col B), and egg color (col C).
%   stopIfUnknownLine - return error if a genetic line cannot be mapped to white or brown color
%                       true | false
%
% OUTPUT
%   data - the table with appended column 'color_group' and values:
%            'white' | 'brown' | 'undetermined'
            
% Example data = map_GeneticLine2Color(data,'20210401 Porphyrio SFA poultrylines_Layer.xlsx', true)


%% Filter flocks based on egg color
% load the mapping of short to long names
[~,lines_txt] = xlsread(filename);
lines_txt = lower(lines_txt);

% add column with line names mapped from short to long version, copy initial name if mapping not available
data.color_group = repmat({'undetermined'},height(data),1);

lines_unique = unique(data.line)';
for line = lines_unique
    pos_2color = strcmp(lower(line),lines_txt(:,1));
    if ~any(pos_2color)
        pos_2color = strcmp(lower(line),lines_txt(:,2));
    end
    if any(pos_2color)
        pos_2data = strcmp(line,data.line);
        data.color_group(pos_2data) = lines_txt(pos_2color,3);
    end
end

pos = strcmp('brown',data.color_group);
disp('BROWN category:')
disp(unique(data.line(pos)))

pos = strcmp('white',data.color_group);
disp('WHITE category:')
disp(unique(data.line(pos)))

pos = strcmp('undetermined',data.color_group);
if any(pos)
    disp('UNDETERMINED color category:')
    disp(unique(data.line(pos)))
else
    disp('All genetics lines mapped into WHITE or BROWN category. No undetermined cases.')
end

if stopIfUnknownLine & any(pos)
    error('Some lines could not be mapped to egg color!')
end