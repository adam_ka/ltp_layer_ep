function data_simulated = simulate_EP_data(model_design,PAR,AGE)

%model_args =  {'design',@(x) [ones(numel(x),1), 1./exp(sqrt(x(:))), x(:), sqrt(x(:))]}
%model_design = model_args{2};

fixed_effects = [87.4387 -424.7018 -0.1065 2.0778]'; % from fitting actual EP data after alinment
genetic_line_effects = [10 50 0.02 0.5; % genetic line #1
                        -15 -50 -0.02 0; % genetic line #2
                        -30 -50 -0.01 -1 ... % genetic line #3
                        ];
days = (1:14:500)';

%% create flocks from different norms and plot them
figure('units','normalized','outerposition',[0.05 0.05 0.9 0.9]);
num_of_flocks_per_line = [30 15 10];
data_simulated = table();
flock_num = 1;
rng(0); % reset random generator for reproducibility
for line = 1:length(num_of_flocks_per_line)
    genetic_line_effect = genetic_line_effects(line,:)';
    for j = 1:num_of_flocks_per_line(line)
        flock_num = flock_num + 1;
        
        noise_std = 0;
        noise = noise_std*randn(size(days));
%         for k = 2:length(noise)
%             noise(k) = 0.5*noise(k-1)+noise(k);
%         end
        flock_effect = 0.3*abs(genetic_line_effect).*(randn(size(genetic_line_effect))-0.5);  % adjust components of the genetic line effect. 
        y_withoutNoise = model_design(days)*(fixed_effects+genetic_line_effect +flock_effect);
        y = y_withoutNoise + noise;
        data_sim = table();
        data_sim.([PAR '_withoutNoise']) = y_withoutNoise;
        data_sim.(PAR) = y;
        data_sim.EP = y;
        data_sim.(AGE) = days;
        data_sim.line = repmat({['norm' num2str(line)']},length(y),1);
        data_sim.flockID = repmat({['line' num2str(line) '_flock' num2str(j)]},length(y),1);
        data_sim.rowNum = repmat({num2str(flock_num)},length(y),1);
        data_sim.client = repmat({'simulated'},length(y),1);
        
        data_simulated = [data_simulated; data_sim];
        
        p = plot(days,y_withoutNoise,'.-');
        p.Annotation.LegendInformation.IconDisplayStyle = 'off';
        %set(get(get(p(2),'Annotation'),'LegendInformation'),'IconDisplayStyle','off');
        hold on
        p = plot(days,y,'.');
        p.Annotation.LegendInformation.IconDisplayStyle = 'off';
    end
end

%% plot fixed effect and norm effects
y = model_design(days)*(fixed_effects);
plot(days,y,'r.-','MarkerSize',20,'DisplayName','Fixed Effect')
hold on
for i = 1:size(genetic_line_effects,1)
    genetic_line_effect = genetic_line_effects(i,:)';
    y = model_design(days)*(fixed_effects+genetic_line_effect);
    plot(days,y,'.-','MarkerSize',20,'DisplayName',['Fixed Effect + Norm #' num2str(i) ' Effect'])
end       
legend('show')
xlabel('Days (aligned)')
ylabel('Egg Production (%)')
title('Simulated EP data: Global Fixed Effect + Line-specific Effects + Flock-specific Effects + Noise')


%{
    'age_days'    'lay_cgalc_fa00_ho�'    'lay_percentage_week'    'EP'    'lay_cgalc_fa00_ho�'    'lay_percentage_we�'    'EP_1'    'flockID'    'rowNum'
    'client'    'line'    'filename'
%}







