classdef LTPLinearMixedModel_withTimeWeight < LTPModel
    properties(SetAccess=private)
        design = @(x) [ones(numel(x),1) x(:)];
        random_design = @(x) [ones(numel(x),1) x(:)];
        random_covariance = [];
        res_var = [];
        fixed_effects = [];
        is_fitted = false;
    end % properties(Hidden)
    properties
        fit_method = 'ML';
    end % properties
    methods
        function obj = LTPLinearMixedModel_withTimeWeight(varargin)
            obj = obj@LTPModel(varargin{:});
            obj.design = obj.parse_option('design',varargin,obj.design);
            obj.random_design = obj.parse_option('random_design',varargin,obj.design);
        end % function
        function [obj,ds] = fit(obj,x,y,g)
            % Check inputs
            in = isfinite(x) & isfinite(y) & isfinite(g);
            [ds,formula] = obj.prepare_fit_data(x(in),y(in),g(in));
            lme_obj = fitlme(ds,formula,'FitMethod',obj.fit_method);
            % Store results
            random_covariance = covarianceParameters(lme_obj);
            obj.random_covariance = random_covariance{1};
            obj.res_var = var(residuals(lme_obj));
            obj.fixed_effects = fixedEffects(lme_obj);
            obj.is_fitted = true;
            % Create optional output
            if nargout>1 % Output table with training data (for debugging or visualization).
                ds.yhat = fitted(lme_obj,'Conditional',true);
                ds.yhat_fixed = fitted(lme_obj,'Conditional',false);
            end % if
        end % function
        function [y,par] = predict(obj,x,x1,y1)
            % Two options for predictions, depending on the number of input arguments:
            % - Calculate predictions with random effects, calculated from (x1,y1):
            %     yhat = obj.predict(x,x1,y1)
            % - Calculate predictions with fixed effects:
            %     yhat = obj.predict(x);
            if ~obj.is_fitted
                error('Model needs to be fitted first!');
            end % if
            X = obj.design(x);
            if nargin==4
                Z = obj.random_design(x);
                in = isfinite(x1) & isfinite(y1);
                random_effects = obj.calculate_random_effects(x1(in),y1(in));
                X = [X Z];
            elseif nargin==2
                random_effects = zeros(size(obj.fixed_effects));
            else
                error('Predict requires 2 or 4 non-empty input arguments!');
            end % if
            par = [obj.fixed_effects;random_effects];
            %<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<AK
            %y = X*par(1:size(X,2));
            fixed_horizon = true
            if fixed_horizon
                yfixed = X(:,1:end/2)*par(1:end/2);
                yrandom = X(:,end/2+1:end)*par(end/2+1:end);
                horizon_days = 90; %90 | inf
                horizon_slope = 60;
                horizon_max_drop = 0.2; %allowed maximum drop in MM contribution within horizon
                horizon_correction_for_slope = -(-horizon_slope*log(1/horizon_max_drop-1));
                horizon_factor = 1-1./(1+exp(-(x-x1(end)-horizon_days-horizon_correction_for_slope)/horizon_slope));
                y = yfixed + yrandom.*horizon_factor(:);
            else
                yfixed = X(:,1:end/2)*par(1:end/2);
                yrandom = X(:,end/2+1:end)*par(end/2+1:end);
                horizon_days = 90+x1(end)/500*90;
                horizon_slope = 60/90*horizon_days;
                horizon_max_drop = 0.2; %allowed maximum drop in MM contribution within horizon
                horizon_correction_for_slope = -(-horizon_slope*log(1/horizon_max_drop-1));
                horizon_factor = 1-1./(1+exp(-(x-x1(end)-horizon_days-horizon_correction_for_slope)/horizon_slope));
                y = yfixed + yrandom.*horizon_factor(:);                
            end
            %>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>AK
        end % function
        function [ds,formula] = prepare_fit_data(obj,x,y,g)
            ds = table(x,y,g); % Doesn't work in R2013a, but fitlme (below) doesn't either
            X = obj.design(x);
            num_fixed_effects = size(X,2);
            formula = '-1'; % design matrix X should have intercept, so don't add here.
            for k = 1:num_fixed_effects
                name = ['x' num2str(k)];
                ds.(name) = X(:,k);
                formula = [formula '+' name];
            end % for
            Z = obj.random_design(x);
            num_random_effects = size(Z,2);
            random_formula = '-1'; % design matrix X should have intercept, so don't add here.
            for k = 1:num_random_effects
                name = ['x' num2str(num_fixed_effects+k)];
                ds.(name) = Z(:,k);
                random_formula = [random_formula '+' name];
            end % for
            formula = ['y~' formula '+(' random_formula '|g)'];
        end % function
        function random_effects = calculate_random_effects(obj,x,y)
            Z = obj.random_design(x);
            if size(Z,1)<size(Z,2)
                error('Not enough data points for calculating random effects!');
            end % if
            GZ = obj.random_covariance*Z';
            R = obj.res_var*eye(size(Z,1));
            %<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<AK
            random_effects = GZ*inv(Z*GZ+R)*(y-obj.design(x)*obj.fixed_effects); % Verbeke
            % GZ*inv(Z*GZ+R) - values of the resulting vector 1xN increase
            % as the number of data points decrease, so that sigle point
            % can have as much effect as multiple points.
            %random_effects = random_effects.*[1,x(end)/500,x(end)/500,x(end)/500]';  big error
            %random_effects = GZ*inv(Z*GZ+R)*(y-obj.design(x)*obj.fixed_effects)*x(end)/500; % AK
            %random_effects = GZ*inv(Z*GZ+R)*(y-obj.design(x)*obj.fixed_effects)*x(end)/250; % larger error than x(end)/500
            %random_effects = GZ*inv(Z*GZ+R)*((y-obj.design(x)*obj.fixed_effects).*x/500); % Verbeke
            %random_effects = GZ*inv(Z*GZ+R)*((y-obj.design(x)*obj.fixed_effects).*x/250); % Verbeke
            %random_effects = GZ*inv(Z*GZ+R)*((y-obj.design(x)*obj.fixed_effects).*0/500); % Verbeke
            %<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<AK
        end % function
        function num = num_pars(obj)
            num = size(obj.random_design(0),2);
        end % function
        function s = to_struct(obj,varargin)
            s = to_struct@LTPModel(obj,varargin{:});
            s.design = func2str(obj.design);
            s.random_design = func2str(obj.random_design);
            s.D1 = obj.random_covariance;
            s.Epsilon = {obj.res_var};
            s.fix_effects = obj.fixed_effects;
        end % function
    end % methods
    methods(Static)
        function obj = from_struct(s,varargin)
            obj = from_struct@LTPModel(s,varargin{:});
            obj.design = str2func(s.design);
            obj.random_design = str2func(s.random_design);
            obj.random_covariance = s.D1;
            obj.res_var = s.Epsilon{1};
            obj.fixed_effects = s.fix_effects;
            if ~isempty(obj.fixed_effects)
                obj.is_fitted = true;
            end % if
        end % function
    end % methods(Static)
end % classdef