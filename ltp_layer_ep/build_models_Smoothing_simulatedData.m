function build_models()
% Build and test linear mixed effects model for layer Egg Production (EP)
% Use moving mean to smooth:
%   - training data
%   - past data when making predictions
% 
% Bussiness expectations
%  LTP-lay EP [-1%, +1%] after peak (weeks 30-35) horizon 2-3 months
%  (https://porphyrio.atlassian.net/wiki/spaces/DSgroup/pages/1917517934/LTP+-Business+expected+performances)
%
% tested with MATLAB R2016a
%
%{
 TODO:
Simulated data:
- test if you can recover true fixed effects under unbalanced genetic lines
data
- effect of noise
- getting noise level



- sort slides by rms of residuals
- estimate measurement noise

y~-1+x1+x2+x3+(-1+x4+x5+x6|g)


%}

global FIGURE_FONT_SIZE
FIGURE_FONT_SIZE = 16

% OUTPUT
model_name = 'LTPLinearMixedModel';

%{
% Mixed model for Layer EP in current LI_SG v1.3.3
% Layers_all_lines_EP_Week_daily_base_model_B_1.mat - for all genetics,
%   both white & brown
model_args =  {'design',@(x) [ones(numel(x),1), 1./exp(sqrt(x(:))), x(:), sqrt(x(:))]}
ppt_filename = 'layer_LTP_EP_LI_SG_1.3.3.pptx';
%}

%{ 
% Lokhorst 1996, Poultry Sci Feed Intake
%model_args =  {'design',@(x) [121.513./(1+0.434*exp(-121.513*4.027e-4*x(:))) -0.028*x(:) 1.617e-5*x(:).^2]} % Lokhorst 1996, Poultry Sci
% Lokhorst - customized 
model_args =  {'design',@(x) [ones(numel(x),1), 121.513./(1+0.434*exp(-121.513*4.027e-4*x(:))), 121.513./(1+0.434*exp(-121.513*4.027e-4*(x(:)-450)/25))]}
model_args =  {'design',@(x) [ones(numel(x),1), 121.513./(1+0.434*exp(-121.513*4.027e-4*(x(:)-0)/0.5)),...
                                                121.513./(1+0.434*exp(-121.513*4.027e-4*(x(:)-350)/50))]} %,...
                                                %121.513./(1+0.434*exp(-121.513*4.027e-4*(x(:)-600)/5))]}
%model_args =  {'design',@(x) [ones(numel(x),1), 121.513./(1+0.434*exp(-121.513*4.027e-4*x(:)))]}
ppt_filename = 'layer_LTP_EP_Lokhorst_modified.pptx';
%}

%%{ 
% Lokhorst 1996, Poultry Sci Egg Production
% original formula
% model_args =  {'design',@(x) [-7.506*ones(numel(x),1), 100./(1+5.274*(0.871.^x(:))), 0.005*x(:), -1.252e-4*x(:).^2]}
  model_args =  {'design',@(x) [-7.506*ones(numel(x),1), 100./(1+      (0.871.^x(:))), 0.005*x(:), -1.252e-4*x(:).^0.5]}
%model_args =  {'design',@(x) [-7.506*ones(numel(x),1) + 100./(1+5.274*(0.871.^x(:))) + 0.005*x(:) + -1.252e-4*x(:).^2, ones(numel(x),1), x(:)]}

ppt_filename = 'layer_LTP_EP_Lokhorst_modified.pptx';
ppt_filename = 'layer_LTP_EP_Lokhorst.pptx';
%}


model_version = 'test'; %datestr(now(),'yyyy-mm-dd');
model_file = ['EP_layer_all_' model_version '.mat'];

% OPTIONS
max_age_days = 490 % (=70*7) after alignment to EP (i.e., shifter by 19 weeks (133 days) on average 
                    % Norm max = 650 days (93 weeks)
                    % Norm max = 518 days (74 weeks) after alignment
                    
max_training_days = inf; % max num of days from age_today into the past used for flock prediction
min_fit_range = 7*0; % min num of days from age_today into the past used for single flock prediction
min_obs = 6; % This is a technical limitation of the model fitting, so should be moved there?

Age_today = [61 44 28 11]*7; % after alignment to EP (i.e., shifted by 19 weeks (133 days) on average

Line_color = {'r','g','b','m'};
xlim = [0 max_age_days];

% Initialize PPTX export
if exist(ppt_filename,'file')
    delete(ppt_filename);
end % if

%% Start new presentation
try
    isOpen = exportToPPTX();
catch % add exportToPPTX toolbox to path
    addpath('exportToPPTX');
    isOpen = exportToPPTX();
end % try
    
if ~isempty(isOpen),
    % If PowerPoint already started, then close first and then open a new one
    exportToPPTX('close');
end
try
    exportToPPTX('open',fileName_str);
catch
    exportToPPTX('new','Dimensions',[11 8.5]);
end
% Add first slide with basic info
slideNum = exportToPPTX('addslide');
exportToPPTX('addtext',[datestr(now,'yyyy-mmm-dd') ' DS/AK Training & testing MM']);
exportToPPTX('addnote',mfilename);
%newFile = exportToPPTX('saveandclose',ppt_filename);


%% Get cleaned training data 
%[data,AGE,AGE_DISPLAY_NAME,PAR,NORM,PAR_DISPLAY_NAME,EP,EP_NORM,GROUP,CLIENT,DESCRIPTOR] = get_training_data();
%save('..\data\data_temp','data','AGE','AGE_DISPLAY_NAME','PAR','NORM','PAR_DISPLAY_NAME','EP','EP_NORM','GROUP','CLIENT','DESCRIPTOR')
load('..\data\data_temp')
%data = data(20000:30000,:);
%data = data(1:5000,:);

% Map genetic line to color category for model training
%data = map_GeneticLine2Color(data);
data = map_GeneticLine2Color(data,'20210401 Porphyrio SFA poultrylines_Layer.xlsx', true);
% Select Color category
color_category = 'white'; % 'white', 'brown', 'any'
if any(strcmp(color_category,{'white','brown'}))
    data = data(strcmp(color_category,data.color_group),:);
end

PAR_REFERENCE = '';
PAR_REFERENCE_DISPLAY_INFO = '';

use_simulated_data = false
if use_simulated_data
    % simulated dataclo
    data = simulate_EP_data(model_args{2},PAR,AGE);
    %{
    % Uncomment to calculate prediction error relative to PAR values without noise
    PAR_REFERENCE = [PAR '_withoutNoise'];
    PAR_REFERENCE_DISPLAY_INFO = 'without noise';  
    %}
    % plot simulated data ('aligned')
    slideNum = exportToPPTX('addslide');
    exportToPPTX('addpicture',gcf,'Scale','maxfixed');
    exportToPPTX('addtext','Simulated dataset');
    exportToPPTX('addnote',mfilename);
else
    % plot cleaned data before alignment
    h_fig = figure('units','normalized','outerposition',[0.05 0.05 0.9 0.9]);
    num_flocks = length(unique(data.(GROUP)));
    plot(data.(AGE),data.(PAR),'r.','DisplayName',[num2str(num_flocks) ' ' upper(color_category) ' flocks before alignment & final cleaning']);
    xlabel('Age (Days)')
    ylabel(PAR,'Interpreter','none')
    
    legend('show')
    slideNum = exportToPPTX('addslide');
    exportToPPTX('addpicture',h_fig,'Scale','maxfixed');
    exportToPPTX('addtext',[upper(color_category) ' flocks before alignment & final cleaning']);
    exportToPPTX('addnote',mfilename);

    % Align data to EP=40%
    disp('Aligning data to EP ...')
    Fcns_get_predictions = LI_SG_Layers_EP_get_predictions();
    shift_days = [];
    for k = unique(data.(GROUP))'
        flock = strcmp(data.(GROUP),k);
        x = data.(AGE)(flock);
        y = data.(PAR)(flock);
        x_EP = data.(AGE)(flock);
        y_EP = data.(EP)(flock);
        ynorm = data.(NORM)(flock);
        ynorm_days = data.(AGE)(flock);   
        [y,x,ynorm_new,ynorm_new_days,init_days]=Fcns_get_predictions.align_data(y,x,y_EP,x_EP,ynorm,ynorm_days);
        shift_days(end+1) = init_days;
        data.(AGE)(flock) = x;
        data.(PAR)(flock) = y;
        data.(NORM)(flock) = ynorm_new;
    end
    disp(['Mean shift by ' num2str(nanmean(shift_days)) ' days'])
    %} 
    % remove NaN
    data = data(~isnan(data.(PAR)) & ~isnan(data.(AGE)),:);

    % remove data after max_age_days
    data = data(data.(AGE)<=max_age_days,:);

    % Check flock length and data coverage
    age_days_min = 350; %=50*7
    n_data_min = 5;
    binEdges = linspace(0,age_days_min,n_data_min+1);
    for k = unique(data.(GROUP))'
        flock = strcmp(data.(GROUP),k);
        x = data.(AGE)(flock);
        if ~all(histcounts(x,binEdges))
            data.(AGE)(flock) = nan;
            data.(PAR)(flock) = nan;
        end
    end
    % remove NaN
    data = data(~isnan(data.(PAR)) & ~isnan(data.(AGE)),:);

    %%{
    % Check flock properties
    % Select only flocks that pass both MM and PN checks
    % load initial MM model, to be used for flock checking (selection) before training the
    % final MM model
    %model_struct_0 = load('FE_layer_all_test_20210210 layer_LTP_FE_Lokhorst_225_5.mat');
    %model_0 = LTPModel.from_struct(model_struct_0);
    model_0 = [];
    disp('Checking flock properties ...')
    for k = unique(data.(GROUP))'
        flock = strcmp(data.(GROUP),k);
        x = data.(AGE)(flock);
        y = data.(PAR)(flock);
        ynorm = data.(NORM)(flock);
        ynorm_days = data.(AGE)(flock);           
        [Radjselec,thr_norm,thr_outlier]=deal([],20,90);
        [pred,outliers] = Fcns_get_predictions.check_flock_properties(y,x,model_0,ynorm,ynorm_days,Radjselec,thr_norm,thr_outlier); 
        if pred(2) %~(pred(2) && pred(1))
            y(outliers | isnan(ynorm)) = nan;
            data.(PAR)(flock) = y;
        else
            data.(AGE)(flock) = nan;
            data.(PAR)(flock) = nan;            
        end
    end
    % remove NaN
    data = data(~isnan(data.(PAR)) & ~isnan(data.(AGE)),:);
    %}
end

% copy data for re-use in training and testing MM without genetic line
data_temp = data;

% Get 2nd data set with norms
data2 = data;
PAR2 = NORM;
NORM2 = NORM;
PAR2_DISPLAY_NAME = 'Norm FE1';

%% Adjust multiphasic model params
%{
mp_model = @(PHI,t) PHI(1)*exp(-exp(-PHI(2)*(t/7-PHI(3)/7))) + PHI(4)*exp(-exp(-PHI(5)*(t/7-PHI(6)/7))) %+ PHI(7) not converging with constant term
PHI0 = [1000 0.0879 0 1000 0.0879*3 130];
[group_name,group_start,group] = unique(data.(GROUP));
PHI1 = nlmefit(data.(AGE),data.(PAR),group,[],mp_model,PHI0,'RefineBeta0',false) 
%}

%{
mp_model = @(P,x) P(1)*121.513./(1+0.434*exp(-121.513*4.027e-4*x(:))) + P(2)*121.513./(1+0.434*exp(-121.513*4.027e-4*(x(:)-P(3))/P(4))) + P(5);
P0 = [0.6444 -0.0357 225 5 45.14];

mp_model = @(P,x) P(1)*121.513./(1+0.434*exp(-121.513*4.027e-4*x(:))) + P(2)*121.513./(1+0.434*exp(-121.513*4.027e-4*(x(:)-P(3))/5)) + P(4);
P0 = [0.6444 -0.0357 225 45.14];

mp_model = @(P,x) 0.6444*121.513./(1+0.434*exp(-121.513*4.027e-4*x(:))) + P(1)*121.513./(1+0.434*exp(-121.513*4.027e-4*(x(:)-P(2))/5)) + 45.14;
P0 = [-0.0357 225];
[group_name,group_start,group] = unique(data.(GROUP));
P1 = nlmefit(data.(AGE),data.(PAR),group,[],mp_model,P0,'RefineBeta0',false) 

'RefineBeta0',true

'RefineBeta0',false

%}

%% Train model
%{
% comment this section to disable smoothing
% smooth data for training
window_length = 49; %[days] 49(default), 28
data.PARmovmean = nan(size(data,1),1);
for k = unique(data.(GROUP))'
    flock = strcmp(data.(GROUP),k);
    x = data.(AGE)(flock);
    y = data.(PAR)(flock);
    xs = nan(max(x),1);
    xs(x)=y;
    %xs = movmean(xs,window_length,'omitnan','EndPoints','shrink');
    xs = movmedian(xs,window_length,'omitnan','EndPoints','shrink');
    data.PARmovmean(flock) = xs(x);
end
data.([PAR 'unsmoothed']) = data.(PAR);
data.(PAR) = data.PARmovmean;
%}


%%{
% ----------- MM MM WITH TIME WEIGHT --------------

slideNum = exportToPPTX('addslide');
fprintf('Added slide %d\n',slideNum);
exportToPPTX('addtext',['Linear Mixed Model WITHOUT Genetic Line as Random Effect']);
exportToPPTX('addnote','');

data = data_temp;

% window_length = 0; % no smoothing
% %data.([PAR 'unsmoothed']) = data.(PAR);
% data.([PAR 'unsmoothed']) = data.([PAR '_withoutNoise']);

% Convert string flock id to numeric flock id
[group_name,group_start,group] = unique(data.(GROUP));
model = feval('LTPLinearMixedModel_withTimeWeight',model_args{:});
disp('MM training ...')
tstart_fitMM = datetime('now')
[model,ds] = model.fit(data.(AGE),data.(PAR),group);
tend_fitMM = datetime('now')
tfit_MM = tend_fitMM - tstart_fitMM

h_fig = figure('units','normalized','outerposition',[0.05 0.05 0.9 0.9]);
    line(ds.x,ds.y,'marker','+','linestyle','none','DisplayName',['data, N=' num2str(length(group_name)) ' flocks']);
    line(ds.x,ds.yhat,'marker','o','linestyle','none','color','g','DisplayName','fit with random effects');
    line(ds.x,ds.yhat_fixed,'marker','.','MarkerSize',20,'linestyle','none','color','r','DisplayName','Fixed Effect');
    title(['Training ' func2str(model.design)],'interpreter','none');
    ylabel(PAR_DISPLAY_NAME,'interpreter','none')
    xlabel(AGE,'interpreter','none')
    legend('show');
    set(gca,'xgrid','on','ygrid','on');

slideNum = exportToPPTX('addslide');
    exportToPPTX('addpicture',h_fig,'Scale','maxfixed');
    exportToPPTX('addtext',['Linear Mixed Model training WITHOUT Genetic Line as Random Effect' ]);
    exportToPPTX('addnote','');

% Test model with 1st dataset
data = testing( model,...
                data,...
                [],...
                AGE,AGE_DISPLAY_NAME,PAR,NORM, PAR_DISPLAY_NAME, GROUP, CLIENT, DESCRIPTOR,...
                PAR_REFERENCE,PAR_REFERENCE_DISPLAY_INFO,...
                max_age_days,max_training_days,min_obs,min_fit_range,Age_today,Line_color,xlim);

prediction_performance_summary( data,...
                                Age_today,...
                                AGE,AGE_DISPLAY_NAME,PAR,PAR_DISPLAY_NAME,...
                                PAR_REFERENCE,PAR_REFERENCE_DISPLAY_INFO,...
                                max_training_days,Line_color,xlim,...
                                model)

% ----------- END MM WITH TIME WEIGHT  --------------
%}



%{
% ----------- MM with Genetic Effect --------------

slideNum = exportToPPTX('addslide');
exportToPPTX('addtext',['Linear Mixed Model WITH Genetic Line as Random Effect']);
exportToPPTX('addnote','');

data = data_temp;

% Plot mean PAR per genetic line
[G,lines_ID,age_ID]=findgroups(data.line,data.(AGE));
Y = splitapply(@(x) [mean(x,'omitnan') std(x,'omitnan')], data(:,PAR),G);
figure('units','normalized','outerposition',[0.05 0.05 0.9 0.9]);
for line_ID = unique(lines_ID)'
    pos = strcmp(line_ID,lines_ID);
    x_plot = age_ID(pos);
    y_plot_mean = Y(pos,1);
    y_plot_std  = Y(pos,2);
    %errorbar(x_plot,y_plot_mean,y_plot_std)
    plot(x_plot,y_plot_mean,'.-','DisplayName',line_ID{:})
    hold on
end
legend('show')
xlabel('Days (after alignment)')
ylabel(['Mean ' PAR],'Interpreter','none')
title('Mean PAR value per genetic line')
slideNum = exportToPPTX('addslide');
exportToPPTX('addpicture',gcf,'Scale','maxfixed');
exportToPPTX('addtext',['Mean PAR values per genetic line']);
exportToPPTX('addnote','');

% Create grouping variable g, based flock id and norm
[group_name,group_start,group] = unique(data.(GROUP));
[group_name_line,group_start_line,group_line] = unique(data.line);
group_line_str = group_name_line(group_line);
model = feval('LTPLinearMixedModel_withGeneticLine',model_args{:});
tstart_fitMMGenetic = datetime('now')
[model,ds,genetic_lines] = model.fit(data.(AGE),data.(PAR),{group,group_line_str});
tend_fitMMGenetic = datetime('now')
tfit_MMGenetic = tend_fitMMGenetic - tstart_fitMMGenetic

h_fig = figure('units','normalized','outerposition',[0.05 0.05 0.9 0.9]);
line(ds.x,ds.y,'marker','+','linestyle','none','DisplayName',['data, N=' num2str(length(group_name)) ' flocks']);
line(ds.x,ds.yhat,'marker','o','linestyle','none','color','g','DisplayName','fit with random effects');
line(ds.x,ds.yhat_fixed,'marker','.','MarkerSize',20,'linestyle','none','color','r','DisplayName','Fixed Effect');
hold on
for i = 1:length(genetic_lines)
    genetic_line = genetic_lines{i};
    %line(ds.x,ds.fixed_and_genetic_effects(:,i),'marker','.','MarkerSize',18,'linestyle','none','DisplayName',['Fixed Effect + ' genetic_line]);
    plot(ds.x,ds.fixed_and_genetic_effects(:,i),'marker','.','MarkerSize',18,'linestyle','none','DisplayName',['Fixed Effect + ' genetic_line])
    hold on
end
title(['Training ' func2str(model.design)],'interpreter','none');
ylabel(PAR_DISPLAY_NAME,'interpreter','none')
xlabel(AGE,'interpreter','none')
legend('show');
set(gca,'xgrid','on','ygrid','on');

slideNum = exportToPPTX('addslide');
fprintf('Added slide %d\n',slideNum);
exportToPPTX('addpicture',h_fig,'Scale','maxfixed');
exportToPPTX('addtext',['Linear Mixed Model training WITH Genetic Line as Random Effect']);
exportToPPTX('addnote','');

data = testing_LMEwithGeneticLineGrouping( model,...
                data,...
                [],...
                AGE,AGE_DISPLAY_NAME,PAR,NORM, PAR_DISPLAY_NAME, GROUP, CLIENT, DESCRIPTOR,...
                PAR_REFERENCE,PAR_REFERENCE_DISPLAY_INFO,...
                max_age_days,max_training_days,min_obs,min_fit_range,Age_today,Line_color,xlim);
prediction_performance_summary( data,...
                                Age_today,...
                                AGE,AGE_DISPLAY_NAME,PAR,PAR_DISPLAY_NAME,...
                                PAR_REFERENCE,PAR_REFERENCE_DISPLAY_INFO,...
                                max_training_days,Line_color,xlim,...
                                model)

% ----------- End MM with Genetic Effect --------------
%} 






%{
% ----------- MM WITHOUT Genetic Effect --------------

slideNum = exportToPPTX('addslide');
fprintf('Added slide %d\n',slideNum);
exportToPPTX('addtext',['Linear Mixed Model WITHOUT Genetic Line as Random Effect']);
exportToPPTX('addnote','');

data = data_temp;

% window_length = 0; % no smoothing
% %data.([PAR 'unsmoothed']) = data.(PAR);
% data.([PAR 'unsmoothed']) = data.([PAR '_withoutNoise']);

% Convert string flock id to numeric flock id
[group_name,group_start,group] = unique(data.(GROUP));
model = feval(model_name,model_args{:});
disp('MM training ...')
tstart_fitMM = datetime('now')
[model,ds] = model.fit(data.(AGE),data.(PAR),group);
tend_fitMM = datetime('now')
tfit_MM = tend_fitMM - tstart_fitMM
model.save(model_file);
clear model
model_struct = load(model_file);
model = LTPModel.from_struct(model_struct);

h_fig = figure('units','normalized','outerposition',[0.05 0.05 0.9 0.9]);
    line(ds.x,ds.y,'marker','+','linestyle','none','DisplayName',['data, N=' num2str(length(group_name)) ' flocks']);
    line(ds.x,ds.yhat,'marker','o','linestyle','none','color','g','DisplayName','fit with random effects');
    line(ds.x,ds.yhat_fixed,'marker','.','MarkerSize',20,'linestyle','none','color','r','DisplayName','Fixed Effect');
    title(['Training ' func2str(model.design)],'interpreter','none');
    ylabel(PAR_DISPLAY_NAME,'interpreter','none')
    xlabel(AGE,'interpreter','none')
    legend('show');
    set(gca,'xgrid','on','ygrid','on');

slideNum = exportToPPTX('addslide');
    exportToPPTX('addpicture',h_fig,'Scale','maxfixed');
    exportToPPTX('addtext',['Linear Mixed Model training WITHOUT Genetic Line as Random Effect' ]);
    exportToPPTX('addnote','');

% Test model with 1st dataset
data = testing( model,...
                data,...
                [],...
                AGE,AGE_DISPLAY_NAME,PAR,NORM, PAR_DISPLAY_NAME, GROUP, CLIENT, DESCRIPTOR,...
                PAR_REFERENCE,PAR_REFERENCE_DISPLAY_INFO,...
                max_age_days,max_training_days,min_obs,min_fit_range,Age_today,Line_color,xlim);

prediction_performance_summary( data,...
                                Age_today,...
                                AGE,AGE_DISPLAY_NAME,PAR,PAR_DISPLAY_NAME,...
                                PAR_REFERENCE,PAR_REFERENCE_DISPLAY_INFO,...
                                max_training_days,Line_color,xlim,...
                                model)

% ----------- END MM WITHOUT Genetic Effect --------------
%}

                            
%% Testing on 2nd dataset
%{
data2 = testing(model,...
                data2,...
                LI_SG_BBR_BW_get_predictions(),...
                AGE,AGE_DISPLAY_NAME,PAR2, NORM2, PAR2_DISPLAY_NAME, GROUP, CLIENT, DESCRIPTOR,...
                max_age_days,max_training_days,min_obs,min_fit_range,Age_today,Line_color,xlim);

prediction_performance_summary( data2,...
                                Age_today,...
                                AGE,AGE_DISPLAY_NAME,PAR2,PAR2_DISPLAY_NAME,...
                                max_training_days,Line_color,xlim,...
                                model)
%}

%% Save presentation and close presentation -- overwrite file if it already exists
% Filename automatically checked for proper extension
newFile = exportToPPTX('saveandclose',ppt_filename);

end

%% Get training data
function [data,AGE,AGE_DISPLAY_NAME,PAR,NORM,PAR_DISPLAY_NAME,EP,EP_NORM,GROUP,CLIENT,DESCRIPTOR] = get_training_data()
% GET_TRAINING_DATA reads PROD files and returns table with selected
% parameters, grouping variable, and aliases for frequently used fields in data.

%datafolder = '..\Data\PredictionPerformanceData_20210122\layers_inactive_weekly_filtered_uniqueID_cleanFE'
datafolder = '..\Data\layers_with_norms_old_and_new_param_names'

%{
Params available in LI_SG:
       'lay_cgalc_fa00_hoor_00_00_fpwwav_00'; % EP, weekly param
%}

% Aliases for (frequently used) fields in data
PAR = 'lay_cgalc_fa00_hoor_00_00_fpwwav_00'; % EP, weekly param
PAR_old = 'lay_percentage_week';
NORM = 'EP';
PAR_DISPLAY_NAME = PAR;
PAR_REFERENCE = '';
PAR_REFERENCE_DISPLAY_INFO = '';
EP = 'lay_cgalc_fa00_hoor_00_00_fpwwav_00';
EP_old = 'lay_percentage_week';
EP_NORM = 'EP';
AGE = 'age_days';
AGE_DISPLAY_NAME = 'Age (days)';
GROUP = 'flockID';
CLIENT = 'client';
DESCRIPTOR = 'rowNum';

data = PRODs2table(datafolder,AGE,{PAR,PAR_old,NORM,EP,EP_old,EP_NORM},GROUP,CLIENT,DESCRIPTOR);

%Combine old and new parameter into new 
% Select the parameter with larger number of valid data, if both old and
% new params have data
disp('Merging data from old- and new-style parameters ... ')
for k = unique(data.filename)'
    flock = strcmp(data.filename,k);
    y_old = data.(PAR_old)(flock);
    N_old = sum(~isnan(y_old));
    if N_old > 0
        y_new = data.(PAR)(flock);
        N_new = sum(~isnan(y_new));
        if N_old > N_new
            data.(PAR)(flock) = y_old;
            data.(EP)(flock) = data.(EP_old)(flock); 
        end
    end
end

disp('Removing flocks with duplicate or NaN Age values ...')
for k = unique(data.filename)'
    flock = strcmp(data.filename,k);
    x = data.(AGE)(flock);
    x_diff_unique = unique(diff(x));
    if length(x_diff_unique)~=1
        data(flock,:)=[];
    end
end

%%{
% clean data
disp(['Cleaning the ' PAR ' data ...'])
Fcns_get_predictions = LI_SG_Layers_EP_get_predictions();
for k = unique(data.filename)'
    flock = strcmp(data.filename,k);
    x = data.(AGE)(flock);
    y = data.(PAR)(flock);
    ynorm = data.(NORM)(flock);
    ynorm_days = data.(AGE)(flock);
    y = Fcns_get_predictions.clean_data(y,x,ynorm,ynorm_days);
    data.(AGE)(flock) = x;
    data.(PAR)(flock) = y;
end
%} 

% Find Prod files with duplicate IDs
duplicate_id = {};
for id = unique(data.(GROUP))'
    pos = strcmp(id,data.(GROUP));
    files = unique(data.filename(pos,:));
    if length(files) > 1
        duplicate_id{end+1} = files;
    end
end

% For each set of duplicate flock IDs, find files with least data for a give
% parameter and remove them from dataset.
for i = 1:length(duplicate_id)
    files = duplicate_id{i};
    for j = 1:length(files)
        f = files{j};
        pos = strcmp(f,data.filename);
        y = data.(PAR)(pos);
        duplicate_id_dataCount(i,j) = sum(~isnan(y));
    end
    [~,imax] = max(duplicate_id_dataCount(i,:));
    duplicate_files_to_remove{i} = files(setdiff(1:end,imax));
end
disp('Removing duplicate files with least data ...')
duplicate_files_to_remove = [duplicate_files_to_remove{:,:}]';
for i = 1:length(duplicate_files_to_remove)
    f = duplicate_files_to_remove{i};
    pos = strcmp(f,data.filename);
    data(pos,:) = [];
end

% remove NaN
pos = isnan(data.(PAR));
data = data(~pos,:);
end

%%  plot prediction performance summary
function prediction_performance_summary(data,...
                                        Age_today,AGE,AGE_DISPLAY_NAME,PAR,PAR_DISPLAY_NAME,...
                                        PAR_REFERENCE,PAR_REFERENCE_DISPLAY_INFO,...
                                        max_training_days,Line_color,xlim,...
                                        model)
% INPUTS:
%   PAR_REFERENCE - variable in 'data' table to store 'original'
%                   (reference) values of PAR, e.g. PAR values without noise (in simulated
%                   data) or PAR values before smoothing
%                   This values (if available) are used to calculate the
%                   prediction error:
%                   PRED_ERROR = PAR_REFERENCE - f_pred(PAR)
%                   string | '' 
%   PAR_REFERENCE_DISPLAY_INFO - legend string
         
if ~isempty(PAR_REFERENCE)
    data.(PAR) = data.(PAR_REFERENCE);
end

for p = 1:4
    h_fig(p) = figure('units','normalized','outerposition',[0.05 0.05 0.9 0.9]);
    h_ax(p) = axes();
end % for
for j = 1:length(Age_today)
    ageToday = Age_today(j);
    try
        pred_error = data.([PAR '_pred_' num2str(j)]) - data.(PAR);
    catch
        continue
    end % try
    in = isfinite(pred_error);
    [age,~,age_grp] = unique(data.(AGE)(in));
    mean_measure = grpstats(data.(PAR)(in),age_grp,@mean);
    [mean_pred_error,std_pred_error,num_pred,mean_abs_error] = grpstats(pred_error(in),age_grp,{'mean','std','numel',@(x) mean(abs(x))});
    disp_name = ['Fit=[' num2str(ageToday-max_training_days) ',' num2str(ageToday) '['];
    LineWidth = 2;
    MarkerSize = 8;
    line(h_ax(1),age,mean_measure,'linestyle','-','LineWidth',LineWidth,'marker','*','MarkerSize',MarkerSize,'color',Line_color{j},'DisplayName',[disp_name ' mean']);
    line(h_ax(2),age,mean_pred_error,'linestyle','-','LineWidth',LineWidth,'marker','*','MarkerSize',MarkerSize,'color',Line_color{j},'DisplayName',[disp_name ' error mean']);
    patch(h_ax(2),[age;flipud(age)],[mean_pred_error+std_pred_error;flipud(mean_pred_error-std_pred_error)],...
          Line_color{j},'DisplayName',[disp_name ' error std'],'EdgeColor','none','FaceAlpha',0.1);
    line(h_ax(3),age,num_pred,      'linestyle','-','LineWidth',LineWidth,'marker','*','MarkerSize',MarkerSize,'color',Line_color{j},'DisplayName',[disp_name ' num']);
    line(h_ax(4),age,mean_abs_error,'linestyle','-','LineWidth',LineWidth,'marker','*','MarkerSize',MarkerSize,'color',Line_color{j},'DisplayName',[disp_name ' MAE']);
end % for
for p = 1:4
    xlabel(h_ax(p),AGE_DISPLAY_NAME);
    title(h_ax(p),['PARAMETER = ' PAR_DISPLAY_NAME ', MODEL = ' class(model) ': ' func2str(model.design)],'interpreter','none');
    set(h_ax(p),'xlim',xlim,'xgrid','on','ygrid','on'); %set(h_ax(p),'xlim',xlim,'xtick',0:70:xlim(2),'xgrid','on','ygrid','on');
    legend(h_ax(p),'show','Location','northwest');
end % for
ylabel(h_ax(1),['Mean value of PARAMETER ' PAR_REFERENCE_DISPLAY_INFO],'interpreter','none');
ylabel(h_ax(2),['Prediction Error = Pred( PAR ) - ( PAR ' PAR_REFERENCE_DISPLAY_INFO ')'],'interpreter','none');
%set(h_ax(2),'ylim',[-20 20]);
ylabel(h_ax(3),'Number of flocks');
ylabel(h_ax(4),'Mean Absolute Error');
fig_text = {'Average measurement','Prediction error distribution','Number of data points','Mean Absolute Error'};
for p = [1 3 2 4] %1:4
    slideNum = exportToPPTX('addslide');
    exportToPPTX('addpicture',h_fig(p),'Scale','maxfixed');
    exportToPPTX('addtext',fig_text{p});
    exportToPPTX('addnote','');
end % for

% For each age in the future, calculate spread in predictions obtained at
% different AgeTodays, and plot the mean spread across all flocks
disp('Calculating intra-flock prediction variability ...')
for j = 1:length(Age_today)
    pred_columns{j} = [PAR '_pred_' num2str(j)];
end

ages = unique(data.(AGE));
flocks = unique(data.flockID)';
ranges_T = [table(ages) array2table(nan(length(ages),length(flocks)))];
for i = 1:length(flocks)
    flock = flocks(i);
    pos = strcmp(flock,data.flockID);
    if any(pos)
        pred_ranges = range(data{pos,pred_columns},2); % warning Warning: This concatenation operation includes an empty array with an incorrect number of rows.
        age_ranges = data.(AGE)(pos);
        for j = 1:length(age_ranges)
            ranges_T{ranges_T.ages == age_ranges(j),1+i} = pred_ranges(j);
        end
    end
end
mean_pred_range = mean(ranges_T{:,2:end},2,'omitnan')';
max_pred_range  = max(ranges_T{:,2:end},[],2,'omitnan')';
min_pred_range  = min(ranges_T{:,2:end},[],2,'omitnan')';

figure('units','normalized','outerposition',[0.05 0.05 0.9 0.9]);
plot(ages',[max_pred_range; mean_pred_range; min_pred_range]','*-')
legend({'Max. across flocks','Mean. across flocks','Min. across flocks'},'Location','Northwest')
xlabel('Days')
ylabel('Intra-flock prediction variability (%)')
grid on
slideNum = exportToPPTX('addslide');
exportToPPTX('addpicture',gcf,'Scale','maxfixed');
exportToPPTX('addtext','Intra-flock prediction variability');
exportToPPTX('addnote','');
end

%%
function data = testing(model,...
                        data,...
                        Fcns_get_predictions,...
                        AGE,AGE_DISPLAY_NAME,PAR,NORM,PAR_DISPLAY_NAME, GROUP, CLIENT, DESCRIPTOR,...
                        PAR_REFERENCE,PAR_REFERENCE_DISPLAY_INFO,...
                        max_age_days,max_training_days,min_obs,min_fit_range,Age_today,Line_color,xlim)
% INPUTS:
%   PAR_REFERENCE - variable in 'data' table to store 'original'
%                   (reference) values of PAR, e.g. PAR values without noise (in simulated
%                   data) or PAR values before smoothing
%                   string | '' 
%   PAR_REFERENCE_DISPLAY_INFO - legend string

% Convert string flock id to numeric flock id
[group_name,group_start,group] = unique(data.(GROUP));
client_name = data.(CLIENT)(group_start);
descriptor = data.(DESCRIPTOR)(group_start);
num_units = numel(group_name);

h_fig = figure('units','normalized','outerposition',[0.05 0.05 0.9 0.9]);
for j = 1:length(Age_today)
    data.([PAR '_pred_' num2str(j)]) = nan(size(data,1),1);
end % for

for k = 1:num_units
    %flock = group==k & isfinite(data.(PAR)) & data.(PAR)>=0 & data.(PAR)<100 & data.(AGE)<=max_age_days;
    flock = group==k & isfinite(data.(PAR)) & data.(AGE)<=max_age_days;
    if ~any(flock)
        continue
    end
    x = data.(AGE)(flock);
    y = data.(PAR)(flock);
    if ~isempty(PAR_REFERENCE)
        y_ref = data.(PAR_REFERENCE)(flock);
    end
    ynorm = data.(NORM)(flock);
    ynorm_days = data.(AGE)(flock);
    min_x = min(x);
    max_x = max(x);

    has_pred = false;    
    clf(h_fig);
    h_ax = axes();
    if ~isempty(PAR_REFERENCE)
        line(x,y_ref,'linestyle','none','marker','*','MarkerSize',8,'color','k','DisplayName',['Data Reference - ' PAR_REFERENCE_DISPLAY_INFO]);
    end
    line(x,y,'linestyle',':','marker','.','MarkerSize',8,'color','k','DisplayName','Data Input');
    for j = find(Age_today>min_x+min_fit_range & Age_today<max_x)
        ageToday = Age_today(j);
        fit_range = [max(min_x,ageToday-max_training_days) ageToday];
        fit = x>=fit_range(1) & x<fit_range(2);
        x_fit = x(fit);
        if numel(x_fit)<min_obs
            continue
        end % if
        has_pred = true;
        y_fit = y(fit);  
        
        prd = x>=ageToday;
        yhat = nan(size(y));
        
        if ~isempty(Fcns_get_predictions)
            % clean data and check flock properties
            y_fit = Fcns_get_predictions.clean_data(y_fit,x_fit,ynorm,ynorm_days);
            pos = ~isnan(y_fit);
            x_fit = x_fit(pos);
            y_fit = y_fit(pos);
            pred(1) = 1; %pred = Fcns_get_predictions.check_flock_properties(y_fit,x_fit,model);
            if pred(1)==0
                continue
            end
        end           
        yhat(prd) = model.predict(x(prd),x_fit,y_fit);
        data.([PAR '_pred_' num2str(j)])(flock) = yhat;

        disp_name = ['Fit=[' num2str(fit_range(1)) ',' num2str(fit_range(2)) '['];
        data_disp_name = [disp_name ' Data'];
        pred_disp_name = [disp_name ' Pred'];
        p = 1; %:2 % 1 = full prediction, 2 = after ageToday only
        %line(h_ax(p),x_fit,y_fit,'linestyle','none','marker','*','color',Line_color{j},'DisplayName',data_disp_name);
        LineWidth = 2;
        % show future predictions beyond available data
        line(h_ax(p),max_x:max_age_days,model.predict(max_x:max_age_days,x_fit,y_fit),'linestyle',':','LineWidth',LineWidth,'marker','none','color',Line_color{j},'HandleVisibility','off'); 
        % show future predictions within available data
        line(h_ax(p),fit_range(2):max_x,model.predict(fit_range(2):max_x,x_fit,y_fit),'linestyle','-','LineWidth',LineWidth,'marker','none','color',Line_color{j},'DisplayName',pred_disp_name);
        % show model fitting to the data up to age today
        line(h_ax(1),fit_range(1):fit_range(2),model.predict(fit_range(1):fit_range(2),x_fit,y_fit),'linestyle','--','LineWidth',LineWidth,'marker','none','color',Line_color{j},'HandleVisibility','off'); 
    end % for
    ylim = [-5 5*(1+ceil(max(y)/5))];
    p = 1;%:2
    xlabel(h_ax(p),AGE_DISPLAY_NAME)
    ylabel(h_ax(p),PAR_DISPLAY_NAME,'interpreter','none')
    title(h_ax(p),[PAR_DISPLAY_NAME ' model = ' class(model)],'interpreter','none');
    set(h_ax(p),'xlim',xlim,'xgrid','on','ygrid','on'); %set(h_ax(p),'xlim',xlim,'ylim',ylim,'xtick',0:70:xlim(2),'xgrid','on','ygrid','on');
    legend(h_ax(p),'show','Location','northeast');
    if has_pred
        slideNum = exportToPPTX('addslide');
        exportToPPTX('addpicture',h_fig,'Scale','maxfixed');
        exportToPPTX('addtext',['CLIENT:' client_name{k} ' UNIT:' group_name{k} ' DESCRIPTOR:' descriptor{k} ' MODEL:' class(model)]);
        exportToPPTX('addnote','');
    end % if
end % for
end


%%
function data = testing_LMEwithGeneticLineGrouping(model,...
                        data,...
                        Fcns_get_predictions,...
                        AGE,AGE_DISPLAY_NAME,PAR,NORM,PAR_DISPLAY_NAME, GROUP, CLIENT, DESCRIPTOR,...
                        PAR_REFERENCE,PAR_REFERENCE_DISPLAY_INFO,...
                        max_age_days,max_training_days,min_obs,min_fit_range,Age_today,Line_color,xlim)
% INPUTS:
%   PAR_REFERENCE - variable in 'data' table to store 'original'
%                   (reference) values of PAR, e.g. PAR values without noise (in simulated
%                   data) or PAR values before smoothing
%                   string | '' 
%   PAR_REFERENCE_DISPLAY_INFO - legend string

% Convert string flock id to numeric flock id
[group_name,group_start,group] = unique(data.(GROUP));
client_name = data.(CLIENT)(group_start);
descriptor = data.(DESCRIPTOR)(group_start);
num_units = numel(group_name);

h_fig = figure('units','normalized','outerposition',[0.05 0.05 0.9 0.9]);
for j = 1:length(Age_today)
    data.([PAR '_pred_' num2str(j)]) = nan(size(data,1),1);
end % for

for k = 1:num_units
    %flock = group==k & isfinite(data.(PAR)) & data.(PAR)>=0 & data.(PAR)<100 & data.(AGE)<=max_age_days;
    flock = group==k & isfinite(data.(PAR)) & data.(AGE)<=max_age_days;
    if ~any(flock)
        continue
    end
    x = data.(AGE)(flock);
    y = data.(PAR)(flock);
    genetic_line = data.line(flock);
    if ~isempty(PAR_REFERENCE)
        y_ref = data.(PAR_REFERENCE)(flock);
    end
    ynorm = data.(NORM)(flock);
    ynorm_days = data.(AGE)(flock);
    min_x = min(x);
    max_x = max(x);

    has_pred = false;    
    clf(h_fig);
    h_ax = axes();
    if ~isempty(PAR_REFERENCE)
        line(x,y_ref,'linestyle','none','marker','*','MarkerSize',8,'color','k','DisplayName',['Data Reference - ' PAR_REFERENCE_DISPLAY_INFO]);
    end
    line(x,y,'linestyle',':','marker','.','MarkerSize',8,'color','k','DisplayName','Data Input');
    for j = find(Age_today>min_x+min_fit_range & Age_today<max_x)
        ageToday = Age_today(j);
        fit_range = [max(min_x,ageToday-max_training_days) ageToday];
        fit = x>=fit_range(1) & x<fit_range(2);
        x_fit = x(fit);
        if numel(x_fit)<min_obs
            continue
        end % if
        has_pred = true;
        y_fit = y(fit);  
        
        prd = x>=ageToday;
        yhat = nan(size(y));
        
        if ~isempty(Fcns_get_predictions)
            % clean data and check flock properties
            y_fit = Fcns_get_predictions.clean_data(y_fit,x_fit,ynorm,ynorm_days);
            pos = ~isnan(y_fit);
            x_fit = x_fit(pos);
            y_fit = y_fit(pos);
            pred(1) = 1; %pred = Fcns_get_predictions.check_flock_properties(y_fit,x_fit,model);
            if pred(1)==0
                continue
            end
        end
        yhat(prd) = model.predict(x(prd),x_fit,y_fit,genetic_line(1));
        data.([PAR '_pred_' num2str(j)])(flock) = yhat;

        disp_name = ['Fit=[' num2str(fit_range(1)) ',' num2str(fit_range(2)) '['];
        data_disp_name = [disp_name ' Data'];
        pred_disp_name = [disp_name ' Pred'];
        p = 1; %:2 % 1 = full prediction, 2 = after ageToday only
        %line(h_ax(p),x_fit,y_fit,'linestyle','none','marker','*','color',Line_color{j},'DisplayName',data_disp_name);
        LineWidth = 2;
        % show future predictions beyond available data
        line(h_ax(p),max_x:max_age_days,model.predict(max_x:max_age_days,x_fit,y_fit,genetic_line(1)),'linestyle',':','LineWidth',LineWidth,'marker','none','color',Line_color{j},'HandleVisibility','off'); 
        % show future predictions within available data
        line(h_ax(p),fit_range(2):max_x,model.predict(fit_range(2):max_x,x_fit,y_fit,genetic_line(1)),'linestyle','-','LineWidth',LineWidth,'marker','none','color',Line_color{j},'DisplayName',pred_disp_name);
        % show model fitting to the data up to age today
        line(h_ax(1),fit_range(1):fit_range(2),model.predict(fit_range(1):fit_range(2),x_fit,y_fit,genetic_line(1)),'linestyle','--','LineWidth',LineWidth,'marker','none','color',Line_color{j},'HandleVisibility','off'); 
    end % for
    ylim = [-5 5*(1+ceil(max(y)/5))];
    p = 1;%:2
    xlabel(h_ax(p),AGE_DISPLAY_NAME)
    ylabel(h_ax(p),PAR_DISPLAY_NAME,'interpreter','none')
    title(h_ax(p),[PAR_DISPLAY_NAME ' model = ' class(model)],'interpreter','none');
    set(h_ax(p),'xlim',xlim,'xgrid','on','ygrid','on'); %set(h_ax(p),'xlim',xlim,'ylim',ylim,'xtick',0:70:xlim(2),'xgrid','on','ygrid','on');
    legend(h_ax(p),'show','Location','northwest');
    if has_pred
        slideNum = exportToPPTX('addslide');
        exportToPPTX('addpicture',h_fig,'Scale','maxfixed');
        exportToPPTX('addtext',['CLIENT:' client_name{k} ' UNIT:' group_name{k} ' DESCRIPTOR:' descriptor{k} ' MODEL:' class(model)]);
        exportToPPTX('addnote','');
    end % if
end % for
end
