function Function_handles = LI_SG_Layers_EP_get_predictions()
    Function_handles.main = @main;
    Function_handles.clean_data = @clean_data;
    Function_handles.check_flock_properties = @check_flock_properties;
    Function_handles.align_data = @Layers_align_data;
end

function [ final_matrix, output, prediction_method, final_matrix_vars ] = main( rnd_flock, rnd_flock_days, EP_rnd_flock, EP_rnd_flock_days, ynorm, ynorm_days, ynorm_ep, ynorm_ep_days, model_obj)
 %Predictions for the Egg Production parameter (PAR = 'EP') created by using mixed model (MM), 
 % and/or percentage of EP norm (PN) model.
 % INPUT:
 %  rnd_flock, rnd_flock_days: [Nx1] column vectors with EP data and age
 %  ynorm, ynorm_days: [Mx1] column vectors wiht EP norm data and age
 %  model_obj: MM model object
 %  EP_rnd_flock, EP_rnd_flock_days: alignment data, in this case equal to rnd_flock, rnd_flock_days
 %OUTPUT:
 % final_matrix = matrix including all the EP prediction results 
 % output = matrix including the best predictions of EP for all rnd_flock_days
 %          and its low and up 95% CI (= NaN)
 %
 % 2021-March-22 adam.kapela@evonik.com

% Set parameter name for use in log messages
PAR = 'EP';
 
%% Step 0: check inputs
disp('Step 1: checking inputs...');
%checking PAR data
if (size(rnd_flock,2)~=1) || (size(rnd_flock_days,2)~=1)
    error([' The ' PAR ' data are expected to be a one column array!']);
end
%checking EP data
if (size(EP_rnd_flock,2)~=1) || (size(EP_rnd_flock_days,2)~=1)
    error(' The EP data are expected to be a one column array!');
end

%checking norm data
if (size(ynorm,2)~=1) || (size(ynorm_days,2)~=1)
    error([' The ' PAR ' norm data are expected to be a one column array!']);
end
if (size(ynorm_ep,2)~=1) || (size(ynorm_ep_days,2)~=1)
    error(' The EP norm data are expected to be a one column array!');
end

if any(rnd_flock_days ~= ynorm_days)
    error([' The ' PAR ' and EP norms are expected to have the same size!']);
end

%% Former Step 1: Set parameters
% Radjselec, thr_norm, thr_outlier settings moved inside check_flock_properties()
% thr_EP moved inside align_data()

%% Step 2: Allign data
disp('Step 2: alligning data...');
[rnd_aligned,rnd_aligned_days,ynorm_aligned,ynorm_aligned_days,shift_days] = Layers_align_data(rnd_flock,rnd_flock_days,EP_rnd_flock,EP_rnd_flock_days,ynorm,ynorm_days);
%removing EP outlier
% to be done after allign the norm and the data
num0_NaNs = sum(isnan(rnd_aligned));
rnd_aligned = clean_data(rnd_aligned,rnd_aligned_days,ynorm_aligned,ynorm_aligned_days);
num_NaNs = sum(isnan(rnd_aligned)) - num0_NaNs;
if num_NaNs > 0
    disp([num2str(num_NaNs) ' ' PAR ' data identified as outliers are set to NaN']);
end

%% Step 3: Check flock properties
disp('Step 3: checking flock properties...');
if ~isnan(shift_days)
    [rnd_flock,rnd_flock_days,ynorm,ynorm_days] = deal(rnd_aligned,rnd_aligned_days,ynorm_aligned,ynorm_aligned_days);
    pred = check_flock_properties(rnd_flock,rnd_flock_days,model_obj,ynorm,ynorm_days,[],[],[]);
else
    pred = [0 0];
end

%% Step 4: Get predictions
disp('Step 4: getting predictions...');
ypred_mm = nan(size(rnd_flock));
ypred_pn = nan(size(rnd_flock));
lastwarn('');

%Method 1: Mix Model
if (pred(1) == 1)
    [ ypred_mm] = get_MM_predictions(rnd_flock,rnd_flock_days,model_obj);
    [warnmsg] = lastwarn;
    if (~isempty(warnmsg))
        pred(1) = 0;
        lastwarn('');
    end
end

%Method 2: Perc Norm
if (pred(2) == 1)
    [ypred_pn] = get_PN_predictions(rnd_flock,rnd_flock_days,ynorm,ynorm_days);
    [warnmsg] = lastwarn;
    if (~isempty(warnmsg))
        pred(2) = 0;
        lastwarn('');
    end
end

%% Step 4b: Shift aligned data back to production time
if ~isnan(shift_days)
    rnd_flock_days = rnd_flock_days + shift_days;
    ynorm_days        = ynorm_days        + shift_days;
end

%% Step 5: Create final matrix: days, PAR, yhat, ypred_pn, ynorm
disp('Step 4: creating final matrix...');

% create vector with norm data aligned to PAR data
ypred_no = nan(size(rnd_flock_days));
for i=1:length(ypred_no)
    day = rnd_flock_days(i);
    pos = find(ynorm_days==day);
    if ~isempty(pos)
        ypred_no(i) = ynorm(pos);
    end
end
% concatenate into final matrix
final_matrix = horzcat(rnd_flock_days,rnd_flock, ypred_mm, ypred_pn, ypred_no);
final_matrix_vars = {'days', PAR, '__predmm', '__predpn', '__predno'};
disp(' final_matrix has been created.')

%% Step 6: Create prediction Output
disp('Step 6: creating output matrix...');
if (pred(1) == 1)    
    disp(' the output contains the MM predictions.');
    output = horzcat(ypred_mm, nan(numel(ypred_mm),2)); % append CI = NaN
    prediction_method = 'MM';
elseif (pred(2) == 1)
    disp(' the output contains the PN predictions.');
    output = horzcat(ypred_pn, nan(numel(ypred_pn),2));
    prediction_method = 'PN';
else
    disp(' the output contains the norm.');
    output = horzcat(ypred_no, nan(numel(ypred_no),2));
    prediction_method = 'N';   
end
disp(' output columns are: pred, lowCI, upCI'); %lowCI, upCI = NaN's
disp(' output has been created.');

end %main

%% CLEANING
function [rnd_flock] = clean_data(rnd_flock,rnd_flock_days,ynorm,ynorm_days) 
% Set to NaN parameter values that are negative, zero, or outliers (norm
% based)

rnd_flock(find(rnd_flock<0))= NaN; %negative values do not make sense
rnd_flock(find(rnd_flock==0))= NaN; %zero values do not make sense, except for the initial short period

% remove outliers from the norm
% get norms at BW days
norm = interp1(ynorm_days,ynorm,rnd_flock_days,'linear',nan); % nan - return Nan's outside norm days
index_outlier = isoutlier(rnd_flock, norm, 3); 
rnd_flock(index_outlier) = nan;
end

function [ index_outlier ] = isoutlier(data, norm, factor_tot )
if nargin < 3
    fact = 6;
else fact = factor_tot;
end
X = data-norm(1:size(data,1));
mk = median(X(~isnan(X)));
M_d = mad(X,1);
c = -1/(sqrt(2)*erfcinv(3/2));
smad = c*M_d;
tsmad = fact*smad;
index_outlier = (abs(X-mk)>tsmad);
end

%% Align PAR to EP=40%
function [rnd,rnd_days,ynorm,ynorm_days,shift_days]=Layers_align_data(rnd,rnd_days,EP_rnd,EP_rnd_days,ynorm,ynorm_days)
% LAYERS_ALIGN_DATA shift PAR data and PAR norm leftward so that t = 1 day
%  corresponds to EP=thr_EP (typically 40%).
%  If alignment cannot be done, then all outputs have only NaNs
% OUTPUT:
%   rnd,rnd_days: PAR data with age shifted leftward by 'shift_days'
%   ynorm, ynorm_days : PAR norm with age shifted leftward by 'shift_days'
%   shift_days: [integer] number of days by which output rnd_days where shifted leftward. 
%               [NaN] - EP threshold could not be found.

% User Settings
thr_EP = 40; % EP > 40% is used to align curves
%disp([' thr_EP = ', num2str(thr_EP)]);
negative_days = false; % false/true - remove/keep data points at times <= 0 after the alignment to EP threshold.

% Get the time when EP crosses the threshold 
time_range = [0 280];
init_days = time_of_threshold_crossing(EP_rnd,EP_rnd_days,thr_EP,time_range);
% shift time
if isnan(init_days)
    [rnd(:),rnd_days(:),ynorm(:),ynorm_days(:),shift_days] = deal(nan,nan,nan,nan,nan);
else
    shift_days = init_days - 1;
    rnd_days    = rnd_days - shift_days;
    ynorm_days = ynorm_days  - shift_days;

    if ~negative_days % after alignment, set data at t<=0 to NaN
        pos = rnd_days <= 0;
        rnd(pos)    = nan;
        pos = ynorm_days <= 0;
        ynorm(pos) = nan;
    end
end
end
 
function time_crossing = time_of_threshold_crossing(x,time,x_threshold,time_range)
% TIME_OF_THRESHOLD_CROSSING returns the time when the input signal x
%  crosses the given threshold. 
%  Return Nan, if x does not cross the threshold within the give time range of interest.
% 
% INPUTS:
%  x: [Nx1] input signal
%  t: [Nx1] time points corresponding to the input x
%  x_threshold: [1x1] threshold to be crossed
%  time_range: [1x2] lower and upper limits for the time range of the
%              expected crossing
%
% OUTPUT:
%  time_crossing: [1x1] the estimated (by linear interpolation) the time of
%                       crossing

% remove Nan values
pos = ~isnan(x) & ~isnan(time);
x = x(pos);
time = time(pos);

% Find data point(s) within the required time_range so that s(i)<= threshold and s(n+1)>threshold
x_index = [];
for i = 1:length(x)-1
    x_index(i) = (x(i)<=x_threshold) && (x(i+1)>x_threshold) && (time(i)>=time_range(1)) && (time(i) <= time_range(2));
end
if any(x_index)
    pos = find(x_index,1,'last');
    % interpolate the location of threshold crossing   
    dtime = (time(pos+1) - time(pos))*(x_threshold-x(pos))/(x(pos+1) - x(pos));
    time_crossing = round(time(pos) + dtime);
else
    time_crossing = nan;
end
end

%% CHECK FLOCK
 function [pred, outliers] = check_flock_properties(rnd_flock,rnd_flock_days,model,ynorm,ynorm_days,Radjselec,thr_norm,thr_outlier)
 % Check the properties of the flock to decide which method using for the prediction
 %
 % INPUT:
 %  rnd_flock, rnd_flock_days = flock data and age
 %  model     = MM model object or empty []. If empty [], pred(1)=0 (MM cannot be used)
 %  ynorm,ynorm_days = norm data
 %  Radjselec = minimum required R2adjusted, between MM model and the input flock data
 %              double | [] (use default value)
 % thr_norm  = percentage of the ynorm inside which flock data has to be
 %              double | [] (use default value)
 % thr_outlier = percentage of observations required within the +-thr_norm limits 
 %              double | [] (use default value)
 %
 % OUTPUT: 
 %  pred     = 1x2 = [MM,percNorm], 1 or 0 if the model can be used or not.  
 %  outliers = 1xlength(rnd_flock), 1 where rnd_flock is outside the (norm +- thr_norm) limits. 
 %
 % (See also Layers_EP_check_flock_properties.m & Layers_EP_check_flock_properties.m for the original code for check of FE & EP data)

pred = [0,0];

num_obs = sum(~isnan(rnd_flock));
disp([' The number of obs = ',num2str(num_obs)]);
disp('Check flock properties: setting parameters for EP ...');
if isempty(Radjselec)
    Radjselec = 0.5; % value to check if the prediction can be done with MM
end
disp([' Radjselec = ', num2str(Radjselec)]);
if isempty(thr_norm)
    thr_norm = 20; % value to check if the prediction can be done with Norm
end
disp([' thr_norm = ', num2str(thr_norm)]);
if isempty(thr_outlier)
    thr_outlier = 50; % percentage of obs inside ynorm +- (thr_norm)%.
end
disp([' thr_outlier = ', num2str(thr_outlier)]);

%checking if the PN model can be used
lownorm = ynorm.*(1 - thr_norm/100);
upnorm  = ynorm.*(1 + thr_norm/100);
days_with_data_and_norm = sum(~isnan(rnd_flock) & ~isnan(ynorm));
threshold = double(days_with_data_and_norm)*thr_outlier/100;
outliers = ((rnd_flock < lownorm) | (rnd_flock > upnorm)) & ~isnan(ynorm);
num_outliers = sum(outliers);
if (days_with_data_and_norm > 0) && (days_with_data_and_norm - num_outliers >= threshold)
        disp('For this flock, the Percentage Norm Model can be used');
        pred(2) = 1;
end 

% checking if the MM can be used
if isempty(model)
    disp(' Empty Mixed Model, MM cannot be used');
    return
end
val_flock = 0; 
if (num_obs > 5)
    [rnd_flock_new] = filter_data(rnd_flock,rnd_flock_days,Radjselec,model);
    val_flock = length(rnd_flock_new); 
end
if (val_flock ~= 0 || (num_obs > 0 && num_obs <= 5 && pred(2)==1))
    disp(' For this flock, the Mixed Model can be used');
    pred(1) = 1;
end
end

 %% CHECK MM FIT
 function [rnd_flock_new,rnd_flock_new_days] = filter_data(rnd_flock,rnd_flock_days,Radjselec,model)
 %Data filter 
 %The data filter
 %
 % INPUT:
 % Radjselec= minimum Radj value is needed to be selected for the selection
 % as reference data .9
 %
 %OUTPUT:
 % rnd_flock_new = the body weight data in one array ready for mixed model
 % rnd_flock_new_days = the respective days coupled to rnd_flock_new
 % 
 
pos = ~isnan(rnd_flock);
x = rnd_flock_days(pos);
y = rnd_flock(pos);
xpred = x;
ypred = model.predict(xpred,x,y);          

res = y - ypred;
ymean = mean(y);
ymean_array(1:length(y),1)=ymean;
%RMSE = sqrt((1/(length(days)))*sum(res.^2));
Radj=1-((length(y)-1)/(length(y)-5))*(sum(res.^2)/sum((y-ymean_array).^2)); 
%R2 shows how well terms (data points) fit a curve or line.
%Adjusted R2 also indicates how well terms fit a curve or line, but adjusts for the number of terms in a model:
%-if you add more and more useless variables to a model, adjusted r-squared will decrease.
%-if you add more useful variables, adjusted r-squared will increase. 
%Adjusted R2 will always be less than or equal to R2.   

if (Radj >= Radjselec)
    %creating BW_new and BW_days                
    rnd_flock_new  = y; 
    rnd_flock_new_days = x;
else
    rnd_flock_new  = []; 
    rnd_flock_new_days = [];
end
 end

%% MM PREDICITONS
function [ypred_mm] = get_MM_predictions(y,x,model)
% Use all finite y values to adjust the MM model, and predict y for each x. 

pos = ~isnan(y);
x_fit = x(pos);
y_fit = y(pos);
ypred_mm = model.predict(x,x_fit,y_fit); 
% remove any complex numbers that could be obtained at negative x
pos_real = real(ypred_mm)==ypred_mm; 
ypred_mm(~pos_real) = nan;
end

%% PERCENTAGE NORM PREDICTION
function [ypred_pn] = get_PN_predictions(rnd_flock,rnd_flock_days,ynorm,ynorm_days)
 %Based on Percentage Norm, predict flock parameter after the last valid input until the last rnd_flock_days. 
 %
 % INPUT:
 % rnd_flock, rnd_flock_days: Nx1 Body Weight data and age for which we want prediction
 % ynorm, ynorm_days = BW norm data and age
 
 %OUTPUT:
 % y_npred: Nx1 prediction according to the norm for all days after the last day where there is BW data AND norm data
 %
 % See also: BW_get_PN_predictions() for the original code and calculation of 95% CI
 %           Layers_FE_get_PN_predictions(ds_val, ynorm, Age)
 
 ypred_pn = rnd_flock*nan;
 ypred_pn_days = rnd_flock_days;
 Age = max(rnd_flock_days);
 
 % remove NaN's from BW
 pos = ~isnan(rnd_flock); 
 rnd_flock = rnd_flock(pos);
 rnd_flock_days = rnd_flock_days(pos);
 
 % remove NaN's from norm
 pos = ~isnan(ynorm); 
 ynorm = ynorm(pos);
 ynorm_days = ynorm_days(pos);
  
 % calculate BW - norm
 num = rnd_flock(ismember(rnd_flock_days,ynorm_days)) - ynorm(ismember(ynorm_days,rnd_flock_days));
 den = ynorm(ismember(ynorm_days,rnd_flock_days));

 y_npred_days = intersect(max(rnd_flock_days)+1:Age,ynorm_days);
 for dd = y_npred_days'
     correction = sum(num./den .* ( 1 ./ ( dd - rnd_flock_days(ismember(rnd_flock_days,ynorm_days)))))/sum( 1 ./ ( dd - rnd_flock_days(ismember(rnd_flock_days,ynorm_days))));
     y_npred = ynorm(ynorm_days==dd)* (1 + correction);

     ypred_pn(ypred_pn_days==dd) = y_npred;
 end 
end