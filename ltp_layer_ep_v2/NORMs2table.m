function T = NORMs2table(NORM_name,save_plots_to_ppt)
% Load norms from multiple tabs in given xls file
% Make sure that the xls file has set decimal separator = '.', and thousands separator = ',' (file/Options/Advanced tab)
% INPUT:
%  NORM_str - string of norm name,
%             '' - use default value of 'EP'

if isempty(NORM_name)
    NORM = 'EP';
else
    NORM = NORM_name;
end
filename_norms = '..\data\Layer.xlsx'
ppt_filename = ['plots_norms_' NORM '.pptx'];
%norm_codes = {} % load all norms (from all tabs)
%%{ 
norm_codes = {
    'AA_P';
    'BovBBio';
    'DekAm';
    'DekB';
    'IsaBAlt';
    'IsaBCage';
    'IsaW';
    'LohB_Voliere';
    'LohBLi';
    'LohBLi_Voliere';
    'LohLSLLi';
    'LohSa';
    'LohTrAlt';
    'NovB';
    'NovWCage';
    'NovWLi';
    'TetraSL';
    'TetraSLLL'
    }
%}

%% Start new presentation
if save_plots_to_ppt
    try
        isOpen = exportToPPTX();
    catch % add exportToPPTX toolbox to path
        addpath('exportToPPTX');
        isOpen = exportToPPTX();
    end % try
    
    if ~isempty(isOpen),
        % If PowerPoint already started, then close first and then open a new one
        exportToPPTX('close');
    end
    try
        exportToPPTX('open',fileName_str);
    catch
        exportToPPTX('new','Dimensions',[11 8.5]);
    end
end
%% loop over all tab in xlsx
T = table();
if isempty(norm_codes)
    [~,norm_codes] = xlsfinfo(filename_norms);
end
for k = 1:numel(norm_codes)
    norm_code = norm_codes{k};
    try
        [~,~,raw] = xlsread(filename_norms,norm_code);
    catch
        warning([norm_code ' not available! Skipping to next norm'])
        continue
    end
    pos_Days = strcmpi('Days',raw(1,:));
    pos_NORM   = strcmpi(NORM,raw(1,:));
    if ~any(pos_NORM)
        warning([NORM ' missing in ' norm_code])
        continue
    end
    Days = cell2mat(raw(2:end,pos_Days));
    y_raw = raw(2:end,pos_NORM);
    y_NORM = nan(size(y_raw));
    for i = 1:length(y_raw)
        if isempty(y_raw{i})
            y_NORM(i,1) = nan;
        elseif isstr(y_raw{i})
            if isempty(str2num(y_raw{i}))
                y_NORM(i,1) = nan;
            else
                ystr = strrep(y_raw{i},',','.')
                y_NORM(i,1) = str2num(ystr);
            end
        else
            y_NORM(i,1) = y_raw{i};
        end
    end
    if all(isnan(y_NORM))
        warning([NORM ' empty in ' norm_code])
        continue
    end
    % plot FE norm
    h_fig = figure;
    plot(Days,y_NORM,'ro-')
    title(norm_code,'Interpreter','none')
    xlabel('Days')
    ylabel([NORM ' norm'])
    
    % append norm to table
    Ttemp = table(Days,y_NORM,repmat({norm_code},length(y_NORM),1),'VariableNames',{'age_days',NORM,'line'});
    T = [T; Ttemp];
    
    % save to ppt
    if save_plots_to_ppt
        slideNum = exportToPPTX('addslide');
        exportToPPTX('addpicture',h_fig,'Scale','maxfixed');
        exportToPPTX('addtext',['Norm ' norm_code]);
        exportToPPTX('addnote','');
    end
end

%% Save presentation and close presentation -- overwrite file if it already exists
% Filename automatically checked for proper extension
if save_plots_to_ppt
    newFile = exportToPPTX('saveandclose',ppt_filename);
end

end