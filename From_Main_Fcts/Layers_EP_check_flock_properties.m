 function [pred] = Layers_EP_check_flock_properties(EP_rnd_flock,Radjselec,model,ynorm,thr_EPnorm,thr_outlier)
 % Check the properties of the flock to decide which method using for the prediction
 % INPUT:
 % EP_rnd_flock  = EP data for the flock under prediction
 % Radjselec     = minimum R2adjsted obtained with the MM model
 % model         = type of model used for MM
 % ynorm      = EP norm curve
 % thr_EPnorm    = percentage of the ynorm inside which EP data has to be
 % thr_outlier   = percentage of observation that hve to be inside the
 %                 limits identified by thr_BW/EPnorm
 %OUTPUT: 
 % pred          = 1X2 array = [MM,percNorm], 1 or 0 if the model can be used or not.   
pred = [0,0];

limitday = find(~(isnan(EP_rnd_flock)),1, 'last'); %last week with an obs
val_flock = 0;
num_obs = sum(~isnan(EP_rnd_flock));
if (isempty(limitday))
    limitday = 0;
end
disp([' The number of EP obs = ',num2str(num_obs),' between day ', num2str(find(~(isnan(EP_rnd_flock)),1, 'first')),' and day ', num2str(limitday)]);

%checking if the PN model can be used
days = find(~(isnan(EP_rnd_flock)));
lownorm = ynorm.*(1 - thr_EPnorm/100);
upnorm = ynorm.*(1 + thr_EPnorm/100);
threshold = double(num_obs)*thr_outlier/100;
if (~isempty(days) &&(sum(EP_rnd_flock(days) >= lownorm(days)) >= threshold) &&...
        (sum(EP_rnd_flock(days) <= upnorm(days))>=threshold))
        disp('For this flock, the Percentage Norm Model can be used');
        pred(2) = 1;
end 

% checking if the MM can be used
if (num_obs > 5)
        start = find(isnan(EP_rnd_flock)==0,1,'first'); % starting week
        einde = find(isnan(EP_rnd_flock)==0,1,'last'); % last week with registrations!
        C = [];
        dd = [];            
        dd = find(isnan(EP_rnd_flock(start:einde))==0)+start-1; % in fact these are the real weeks
        y = [];
        y = EP_rnd_flock(dd);
        dd = (dd-min(dd))+1; % this starts from the label "week 1" for any flock now.
        Intercept(dd,1)=1;
        if (model == 'B')  
            C = [Intercept(dd),1./(exp(sqrt(dd))),dd,sqrt(dd)]; % this is the needed data format. So with the 4 columns that are used in the linear mixed model.
        else
            print('unknow model')
        end
        x = lsqlin(C,y);
        res = y-(C*x);
        %disp([res,y,C*x]);
        %%disp(x);
        ymean = mean(y);
        ymean_array = [];
        ymean_array(1:length(dd),1) = ymean;
        Radj = 1-((length(dd)-1)/(length(dd)-5))*(sum(res.^2)/sum((y-ymean_array).^2)); 
        %disp(Radj);
        %visualizing results
%{
        plot(dd,y,'r');
        hold on;
        plot(dd,(C*x),'k--','LineWidth',2);
        xlabel('Days after EP>40%','FontSize',14);
        ylabel('Egg Production','FontSize',14);
        set(gca,'FontSize',14);
        title('Offline estimation for selection','FontSize',18);
        hold off;
        pause;                    
%}
        if (Radj >= Radjselec)
            val_flock = 1; 
        end
end
if (val_flock ~= 0 || (num_obs > 0 && num_obs <= 5 && pred(2)==1))
    disp(' For this flock, the Mix Model can be used');
    pred(1) = 1;
end
