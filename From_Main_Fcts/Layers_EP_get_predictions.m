 function [ final_matrix, output, method ] = Layers_EP_get_predictions( EP_rnd_flock, ynorm, D1, Epsilon, fix, model)
 %Predictions for the EP created by using mixed model (MM), percentage of
 % EP norm model.
 % INPUT:
 % EP_rnd_flock = EP data
 % ynorm = FE norm data
 % D1 = MM covariance parameters
 % Epsilon = MM model variance of residuals 
 % fix_effects = MM model fix effect coefficients
 % model = MM model named according to Bart's presentation, 'L' or 'J'
 %OUTPUT:
 % final_matrix = matrix including all the BW prediction results 
 % output = matrix including the best prediction and its low and up 95% CI for all days after the last day where there is FE data 
%% Step 0: check inputs
disp('Step 0: checking inputs...');
%checking EP data
if(size(EP_rnd_flock,2)~=1)
    error(' The EP data are expected to be a one column array!');
end

%checking norm data
if(size(ynorm,2)~=1)
    error(' The EP norm data are expected to be a one column array!');
end

%checking mixed model parameter
if (size(D1,1)~= size(D1,2))
    error(' D1 is not a squared matrix!');
end
if (size(D1,1)~= size(fix,1))    
    error(' The number of values in fix does not agree with D1 size');
end
%% Step 1: Set parameters
disp('Step 1: setting parameters...');
Radjselec = 0.5; % value to check if the prediction can be done with MM
disp([' Radjselec = ', num2str(Radjselec)]);
thr_EPnorm = 20; % value to check if the prediction can be done with Norm
disp([' thr_FEnorm = ', num2str(thr_EPnorm)]);
thr_outlier = 50; % percentage of obs inside ynorm +- (thr_norm)%. Same value for both FE and BW norm curves
disp([' thr_outlier = ', num2str(thr_outlier)]);
thr_EP = 40; % EP > 40% is used to align curves
disp([' thr_EP = ', num2str(thr_EP)]);
%% Step 2: Allign data
disp('Step 2: alligning data...');
[EP_rnd_flock, ynorm, initweek] = Layers_EP_allign_data(EP_rnd_flock,ynorm,thr_EP);
Age = size(EP_rnd_flock,1);
%removing EP outlier
% to be done after allign the norm and the data
disp(' EP data identified as outliers are set to NaN');
index_outliers = isoutlier(EP_rnd_flock,ynorm,3);

if (~isempty(index_outliers))
    disp(['  There are ', num2str(sum(index_outliers)),...
        ' outlier values of EP']);
    EP_rnd_flock(index_outliers)= NaN;
end
%% Step 3: Check flock properties
%%disp([EP_rnd_flock,ynorm]);
disp('Step 3: checking flock properties...');

[pred] = Layers_EP_check_flock_properties(EP_rnd_flock,Radjselec,model,ynorm,thr_EPnorm,thr_outlier);
%% %% Step 4: Get predictions 
disp('Step 4: getting predictions...');
days = find(~(isnan(EP_rnd_flock))); % index from 1 to Age
[ ds_val ] = Layers_EP_build_dataset( EP_rnd_flock(days), days, ones(length(days),1), model );
ypred_mm = [];
ypred_pn = [];
CIpred_mm = [];
CIpred_pn = [];
lastwarn('');
%fix = [65.0512, -506.4679,-0.7012,8.4610]';
%Method 1: Mix Model
% if there are more than 4 obs but their fit has not Radj high enough, MM predictions are not given 

if (pred(1) == 1)
    [ ypred_mm,CIpred_mm] = Layers_EP_get_MM_predictions(D1,Epsilon,fix,ds_val,model,Age);
    [warnmsg] = lastwarn;
    if (~isempty(warnmsg))
        pred(1) = 0;
        lastwarn('');
    end
end

%Method 2: Perc Norm
if (pred(2) == 1)
    [ ypred_pn,CIpred_pn] = Layers_EP_get_PN_predictions(ds_val, ynorm, Age);    
    [warnmsg] = lastwarn;
    if (~isempty(warnmsg))
        pred(2) = 0;
        lastwarn('');
    end
end  

%% Step 5: Create final matrix: days, FE, BW, yhat, ypred_pn, ynorm, ypred_fe
disp('Step 5: creating final matrix...');
if (isempty(ypred_mm)) %it can be empty or completely filled 1-43 (nothing to do in this case)
    ypred_mm = nan(Age, 1);
    CIpred_mm = nan(Age, 2, 1);
end 
if (length(CIpred_mm) < Age) %...or empty, partially filled 
    CIpred_mm = vertcat(nan(Age-size(CIpred_mm,1),2,1),CIpred_mm);
end   
if (length(ypred_pn) < Age) %...or empty, partially filled 
    ypred_pn = vertcat(nan(Age-size(ypred_pn,1),1),ypred_pn);
    CIpred_pn = vertcat(nan(Age-size(CIpred_pn,1),2,1),CIpred_pn);
end
%ATTENTION: if the col order of the final matrix changes,change output accordingly  
disp(' final_matrix columns are: day, EP, MMpred, MMlowCI, MMupCI, EPnorm, PNpred, PNlowCI, PNupCI');
final_matrix = horzcat([1:Age]',EP_rnd_flock, ypred_mm, CIpred_mm, ynorm(1:Age), ypred_pn, CIpred_pn);

if (initweek > 1)
    final_matrix = vertcat(NaN(initweek-1,size(final_matrix,2)),final_matrix);
end
disp(' final_matrix has been created.')
%% Step 6: Create prediction Output
disp('Step 6: creating output matrix... ');
if (pred(1) == 1) %are MM results in the following position of the final_matrix
    disp(' the output contains the MM predictions.');
    output = final_matrix(:,3:5);
    method = 'MM';
elseif (pred(2) == 1) %are PN results in the following position of the final_matrix
    disp(' the output contains the PN predictions.');
    output = final_matrix(:,7:9);
    method = 'PN';
else
    output = horzcat(final_matrix(:,6), zeros(size(final_matrix,1), 2, 1));
    disp(' the output contains the N predictions.');
    method = 'N';
end
disp(' output columns are: pred, lowCI, upCI');
disp(' output has been created.');
end