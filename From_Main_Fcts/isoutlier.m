function [ index_outlier ] = isoutlier(data, norm, factor_tot )

if nargin < 3
    fact = 6;
else fact = factor_tot;
end
X = data-norm(1:size(data,1));
%X = FE;
mk = median(X(~isnan(X)));
M_d = mad(X,1);
c = -1/(sqrt(2)*erfcinv(3/2));
smad = c*M_d;
tsmad = fact*smad;
index_outlier = (abs(X-mk)>tsmad);
 end
